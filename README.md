# TermColor

TermColor is a script that allows you set the Tilix Terminal theme and Fish colors based on the colors of an image. For example, you can use your desktop wallpaper as an input so your terminal colors will match it. 

## Getting Started

To use the script you need [Python 3](https://www.python.org/downloads/). The library required to run TermColors are listed in `Requirements.txt`. To install them download the project, then move to the project folder and create a [virtual environment](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/). Then you can activate the venv with
```
$ source <env_name>/bin/activate
```
and install the libraries with
```
(<env_name>)$ pip install -r path/to/Requirements.txt
```

## Usage

Script Arguments:

| Argument                   	| Description 											|
| ----------------------------- | ----------------------------------------------------- |
| -i  --image < path/to/image > | The path to the image. 								|
| -f --fish 					| Set the Fish Colors.									|
| -t --tilix 					| Create a Tilix Theme.									|
| -d --tilixDefault 			|Set the Tilix Theme as default. (Must be used with -t)	|
| -h,-help						|Show help.												|

The only argument that must be present is `-i`. If you use it alone the scirpt will show you the selected colors and then you can choose to use them as Tilix theme and/or Fish colors. On the other side if use `-i` with other argument the script will select the colors and set them automatically.


### Example 

If you use `exampleImage.jpg` as an input

![Example Image!](./Images/exampleImage.jpg )

the script shows you this colors:

![Output!](./Images/out.png )

Now, if you like the colors you can choose to use them.

## Built With
* [Visual Studio Code](https://code.visualstudio.com/) - The  code editor.
* [Python](https://www.python.org/) - The language used.

## Versioning
I use [BitBucket](https://bitbucket.org/) for versioning. 

## Author
 **Sara Borriello** 