import colorgram
import numpy as np
import cv2
import sys, os
import subprocess
import getopt


newPalette=""

def hsl_to_rgb(h, s, l):
	''' 
		Convert an hsl color into an rgb color and returns it.
	'''
	def hue_to_rgb(p, q, t):
		t += 1 if t < 0 else 0
		t -= 1 if t > 1 else 0
		if t < 1/6: return p + (q - p) * 6 * t
		if t < 1/2: return q
		if t < 2/3: p + (q - p) * (2/3 - t) * 6
		return p
	if s == 0:
		r, g, b = l, l, l
	else:
		q = l * (1 + s) if l < 0.5 else l + s - l * s
		p = 2 * l - q
		r = hue_to_rgb(p, q, h + 1/3)
		g = hue_to_rgb(p, q, h)
		b = hue_to_rgb(p, q, h - 1/3)
	return round(r*255), round(g*255), round(b*255)



def colorExtraction(imagePath):
	'''
		Takes an image and extract 8 colors. Then divides them in  more and less relevant.
		It also generate 2 more colors (This are used just with Fish)
	'''
	numColors=8
	colors = colorgram.extract(imagePath, numColors)
	if(len(colors)<numColors):
		print("Not enough colors in the image")
		exit()
	colors.sort(key=lambda c: c.rgb.r + c.rgb.g + c.rgb.b)
	
	colorsRel=[]
	colorsNotRel=[]
	for i in range(0, numColors//2):
		if (colors[i*2].proportion > colors[i*2+1].proportion):  
			colorsRel.append(colors[i*2])
			colorsNotRel.append(colors[i*2+1])
		else:
			colorsRel.append(colors[i*2+1])
			colorsNotRel.append(colors[i*2])

	y = int(0.2125 *colorsNotRel[3].rgb.r + 0.7154 *colorsNotRel[3].rgb.g + 0.0721 *colorsNotRel[3].rgb.b)
	colorsNotRel.append(colorgram.Color(y, y, y, 0))

	if(colorsNotRel[2].hsl.h<15 or colorsNotRel[2].hsl.h>220):
		r,g,b=hsl_to_rgb(0.7,colorsNotRel[2].hsl.s/255,colorsNotRel[2].hsl.l/255)
	else:
		r,g,b=hsl_to_rgb(0,colorsNotRel[2].hsl.s/255,colorsNotRel[2].hsl.l/255)
	colorsNotRel.append(colorgram.Color(r, g, b, 0))

	return (colorsRel, colorsNotRel)


def setFishColors(home, colorsRel, colorsNotRel):
	fishFile = open(home+"/.config/fish/fish_variables", "r+")  
	lines = fishFile.readlines()

	for i in range(0, len(lines)):
		if("fish_color_cwd:" in lines[i]):
			lines[i]="SETUVAR fish_color_cwd:"+"%02x%02x%02x\n" % colorsRel[2].rgb
		if("fish_color_user:" in lines[i]):
			lines[i]="SETUVAR fish_color_user:"+"%02x%02x%02x\n" % colorsRel[3].rgb
		if("fish_color_command:" in lines[i]):
			lines[i]="SETUVAR fish_color_command:"+"%02x%02x%02x\n" % colorsNotRel[2].rgb
		if("fish_color_operator:" in lines[i]):
			lines[i]="SETUVAR fish_color_operator:"+"%02x%02x%02x\n" % colorsNotRel[2].rgb
		if("fish_color_param:" in lines[i]):
			lines[i]="SETUVAR fish_color_param:"+"%02x%02x%02x\n" % colorsNotRel[3].rgb
		if("fish_color_autosuggestion:" in lines[i]):
			lines[i]="SETUVAR fish_color_autosuggestion:"+"%02x%02x%02x\n" % colorsNotRel[4].rgb
		if("fish_color_error:" in lines[i]):
			lines[i]="SETUVAR fish_color_error:"+"%02x%02x%02x\n" % colorsNotRel[5].rgb 
	fishFile.seek(0)
	fishFile.writelines(lines)
	fishFile.close()


def createTilixTheme(home, defaultTilixProf,colorsRel):
	global newPalette
	tilixFile = open(home+"/.config/tilix/schemes/TermColorTheme.json", "w+")
	tilixFile.write('{\n\t"name": "TermColorTheme",\n\t"comment": "made with term color",')
	tilixFile.write('\n\t"foreground-color": "#%02x%02x%02x' % colorsRel[3].rgb+'",')
	tilixFile.write('\n\t"background-color": "#%02x%02x%02x' % colorsRel[0].rgb+'",\n\t"use-theme-colors": false,\n\t"bold-is-bright": false,\n\t"palette": [')
	
	defaultTilixPalette= subprocess.run(['dconf', 'read', '/com/gexperts/Tilix/profiles/'+defaultTilixProf.stdout.decode('utf-8')[1:-2]+'/palette'], stdout=subprocess.PIPE).stdout.decode('utf-8')
	
	palette=defaultTilixPalette[1:-2].split(", ")
	palette[4]="'#%02x%02x%02x'" % colorsRel[1].rgb #palette[12]
	palette[6]="'#%02x%02x%02x'" % colorsRel[2].rgb
			
	newPalette=""
	for s in palette:
		newPalette+=('\n\t\t"'+s[1:8]+'",')
	newPalette=newPalette[:-1]
	tilixFile.write(newPalette+"\n\t]\n}")


def setTilixThemeDefault(defaultTilixProf, colorsRel):
	subprocess.run(['dconf', 'write', '/com/gexperts/Tilix/profiles/'+defaultTilixProf.stdout.decode('utf-8')[1:-2]+'/background-color', "'#%02x%02x%02x'" % colorsRel[0].rgb], stdout=subprocess.PIPE)
	subprocess.run(['dconf', 'write', '/com/gexperts/Tilix/profiles/'+defaultTilixProf.stdout.decode('utf-8')[1:-2]+'/foreground-color', "'#%02x%02x%02x'" % colorsRel[3].rgb], stdout=subprocess.PIPE)
	subprocess.run(['dconf', 'write', '/com/gexperts/Tilix/profiles/'+defaultTilixProf.stdout.decode('utf-8')[1:-2]+'/bold-is-bright', "false"], stdout=subprocess.PIPE)
	subprocess.run(['dconf', 'write', '/com/gexperts/Tilix/profiles/'+defaultTilixProf.stdout.decode('utf-8')[1:-2]+'/palette', "["+newPalette+"]"], stdout=subprocess.PIPE)
	subprocess.run(['dconf', 'write', '/com/gexperts/Tilix/profiles/'+defaultTilixProf.stdout.decode('utf-8')[1:-2]+'/visible-name', "'TermColorTheme'"], stdout=subprocess.PIPE)


def ShowColors(home, colorsRel, colorsNotRel):	
	if(os.path.isfile(home+"/.config/fish/fish_variables")):
		img= np.full((500,560,3), 255,np.uint8)

		cv2.putText(img,org=(300,80), text="F. command", fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0,0,0))
		cv2.putText(img,org=(300,200), text="F. param", fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0,0,0))
		cv2.putText(img,org=(300,320), text="F. suggest.", fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0,0,0))
		cv2.putText(img,org=(300,440), text="F. error", fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0,0,0))
		for i in range(2,len(colorsNotRel)):
			cv2.rectangle(img,(440,120*(i-2)+20),(540,120*(i-2)+120),(colorsNotRel[i].rgb.b, colorsNotRel[i].rgb.g, colorsNotRel[i].rgb.r ), -1 )
	else:
		img= np.full((500,280,3), 255,np.uint8)

	cv2.putText(img,org=(20,80), text="Backgorund", fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0,0,0))
	cv2.putText(img,org=(20,200), text="Directory", fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0,0,0))
	cv2.putText(img,org=(20,320), text="Path", fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0,0,0))
	cv2.putText(img,org=(20,440), text="Foreground", fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0,0,0))

	for i in range(len(colorsRel)):
		cv2.rectangle(img,(160,120*i+20),(260,120*i+120),(colorsRel[i].rgb.b, colorsRel[i].rgb.g, colorsRel[i].rgb.r ), -1 )

	print("\n\nPress a key to continue")
	cv2.imshow('colors',img)
	cv2.waitKey(0)
	cv2.destroyAllWindows()


def main(argv):
	usage=("Usage: \n  TermColor.py -i <imagePath> [-f] [-t [-d]]"+
		"\n\nOptions:\n  "+
		"-h,\t--help\t\tShow help.\n  "+
		"-i,\t--image <path>\tPath to the image.If you specify just this param the script'll guide you.\n  "+
		"-f,\t--fish \t\tSet the Fish Colors.\n  "+
		"-t,\t--tilix \t\tCreate a Tilix Theme.\n  "+
		"-d,\t--tilixDefault \tSet the Tilix Theme as default. (Must be used with -t)\n  ")

	imagePath=None
	setFish=False
	setTilix=False
	setTilixDefault=False
	NoParam=True

	try:
		opts, args = getopt.getopt(argv,"i:ftdh",["image=","fish", "tilix", "tilixDefault", "help"])
	except getopt.GetoptError:
		print (usage)
		sys.exit(2)

	for opt, arg in opts:
		if( opt in ("-h","--help")):
			print (usage)
			sys.exit()
		elif (opt in ("-i", "--image")):
			imagePath = arg
		elif (opt in ("-f", "--fish")):
			setFish = True
			NoParam=False
		elif(opt in ("-t","--tilix")):
			setTilix=True
			NoParam=False
		elif(opt in ("-d","--tilixDefault")):
			setTilixDefault=True
			NoParam=False

	if(imagePath==None):
		print("You Must specify an imagePath")
		print (usage)
		sys.exit()
	if(setTilix== False and setTilixDefault==True):
		print("You Must generate a Tilix them to set it as dafault")
		print (usage)
		sys.exit()

	if(os.geteuid() != 0):
		home=os.path.expanduser("~")
	else:
		home=os.path.expanduser("~"+os.environ['SUDO_USER'])

	colorsRel, colorsNotRel = colorExtraction(imagePath)

	#--------------------------------------- Terminal Colors-----------------------------------°
	print("\nColors:")
	print("Backgorund: rgb(%03d, %03d, %03d)" % (colorsRel[0].rgb)+ " #%02x%02x%02x" % colorsRel[0].rgb +" \tproportion:"+ "%.2f"%(colorsRel[0].proportion))
	print("Directory : rgb(%03d, %03d, %03d)" % (colorsRel[1].rgb)+ " #%02x%02x%02x" % colorsRel[1].rgb +" \tproportion:"+ "%.2f"%(colorsRel[1].proportion))
	print("Path      : rgb(%03d, %03d, %03d)" % (colorsRel[2].rgb)+ " #%02x%02x%02x" % colorsRel[2].rgb +" \tproportion:"+ "%.2f"%(colorsRel[2].proportion))
	print("Foreground: rgb(%03d, %03d, %03d)" % (colorsRel[3].rgb)+ " #%02x%02x%02x" % colorsRel[3].rgb +" \tproportion:"+ "%.2f"%(colorsRel[3].proportion))


	#--------------------------------------- Fish Colors-------------------------------------------------------#
	if(os.path.isfile(home+"/.config/fish/fish_variables")):
		print("\nFish command/operator: rgb(%03d, %03d, %03d)" % (colorsNotRel[2].rgb) + " #%02x%02x%02x" % colorsNotRel[2].rgb +" proportion:"+ "%.2f"%(colorsNotRel[2].proportion))
		print("Fish param           : rgb(%03d, %03d, %03d)" % (colorsNotRel[3].rgb) + " #%02x%02x%02x" % colorsNotRel[3].rgb +" proportion:"+ "%.2f"%(colorsNotRel[3].proportion))
		print("Fish autosuggestion  : rgb(%03d, %03d, %03d)" % (colorsNotRel[4].rgb) + " #%02x%02x%02x" % colorsNotRel[4].rgb +" proportion:"+ "%.2f"%(colorsNotRel[4].proportion))
		print("Fish error	     : rgb(%03d, %03d, %03d)" % (colorsNotRel[5].rgb) + " #%02x%02x%02x" % colorsNotRel[5].rgb +" proportion:"+ "%.2f"%(colorsNotRel[5].proportion))


	#--------------------------------------------Show colors -------------------------------------------------#
	if(NoParam):
		ShowColors(home,colorsRel,colorsNotRel)

		text=input("\nDo you want to set these colors? (y/n)")
		if(text!="y" and text!="Y"):
			exit()

		#---------------------------------------- change fish color -----------------------------------# 
		if(os.path.isfile(home+"/.config/fish/fish_variables")):
			text=input("\nDo you want to set fish colors? (y/n)")
			if(text=="y" or text=="Y"):
				setFishColors(home, colorsRel,colorsNotRel)
				print("File "+home+"/.config/fish/fish_variables modified")
				print("Fish colors changed")

		#-------------------------------------------- Tilix ------------------------------------#
		defaultTilixProf= subprocess.run(['dconf', 'read', '/com/gexperts/Tilix/profiles/default'], stdout=subprocess.PIPE)
		if(defaultTilixProf.stdout.decode('utf-8')!= ''):
			text=input("\nDo you want to create a tilix theme? (y/n)")
			if(text=="y" or text=="Y"):
				createTilixTheme(home,defaultTilixProf, colorsRel)
				print("File "+home+"/.config/tilix/schemes/TermColorTheme.json created")

				text=input("\nDo you want to set the new tilix theme as default? (y/n)")
				if(text=="y" or text=="Y"):
					setTilixThemeDefault(defaultTilixProf,colorsRel)
					primt("Default theme setted")

	else:
		if(os.path.isfile(home+"/.config/fish/fish_variables") and setFish):
			setFishColors(home, colorsRel,colorsNotRel)
			print("\nFile "+home+"/.config/fish/fish_variables modified")
			print("Fish colors changed")
		
		defaultTilixProf= subprocess.run(['dconf', 'read', '/com/gexperts/Tilix/profiles/default'], stdout=subprocess.PIPE)
		if(defaultTilixProf.stdout.decode('utf-8')!= '' and setTilix):
			createTilixTheme(home,defaultTilixProf, colorsRel)
			print("\nFile "+home+"/.config/tilix/schemes/TermColorTheme.json created")
			if(setTilixDefault):
				setTilixThemeDefault(defaultTilixProf,colorsRel)
				print("\nDefault theme setted")


if __name__ == '__main__':
	 main(sys.argv[1:])
